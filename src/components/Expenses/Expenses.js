import React, {useState} from 'react';

import ExpensesList from './ExpensesList';
import Card from '../UI/Card';
import './Expenses.css';
import ExpensesFilter from "./ExpensesFilter";
import ExpensesChart from './ExpensesChart';

const Expenses = (props) => {
  const [filteredYear, setFilteredYear] = useState("all");

  const filterChangeHandler = (selectedYear) => {
    setFilteredYear(selectedYear);
  };
  
  let filteredExpense = props.items;
    if (filteredYear !== 'all') {
    filteredExpense = props.items.filter(item => {
      return (
        item.date.getFullYear() === parseInt(filteredYear)
      )
    })
  }
  
  return (
    <Card className="expenses">
    <ExpensesFilter selected={filteredYear} onChangeFilter={filterChangeHandler}/>
    <ExpensesChart expenses={filteredExpense} />
    <ExpensesList items={filteredExpense} />
    </Card>
  );
};


export default Expenses;
